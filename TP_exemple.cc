// Programme source exemple pour le TP d'Architectures specialisees M1
// informatique Universite de Toulouse 3 - Paul Sabatie 2017
//
// a compiler avec le Makefile fourni : make TP_exemple

#include <iostream>
#include <opencv/cv.h>
#include <opencv/highgui.h>
// #include <xmmintrin.h>	// SSE
// #include <emmintrin.h>	// SSE2
// #include <pmmintrin.h>	// SSE3
#include <tmmintrin.h> // SSSE3

#include <iomanip>

using namespace std;

// affiche_xmm_i() -----------------------------------------------------//
// affiche le contenu du registre xmm entier sous differents format :	//
//  8, 16 et 32 bits non signes.					//
//----------------------------------------------------------------------//

void affiche_xmm_i(__m128i x)
{
  unsigned char tab_u_char[16];
  unsigned short *tab_u_short;
  unsigned int *tab_u_int;
  int i;

  _mm_store_si128((__m128i *)tab_u_char, x);

  cout << "unsigned char" << "\n";
  for (i = 0; i < 16; i++)
  {
    cout << (unsigned int)tab_u_char[i] << " ";
  }
  cout << "\n";

  tab_u_short = (unsigned short *)tab_u_char;
  cout << "unsigned short"<< "\n";
  for (i = 0; i < 8; i++)
  {
    cout << tab_u_short[i] << " ";
  }
  cout << "\n";

  tab_u_int = (unsigned int *)tab_u_short;
  cout << "unsigmed int" << "\n";
  for (i = 0; i < 4; i++)
  {
    cout << tab_u_int[i] << " ";
  }
  cout << std::endl;
}

// Inverse() -----------------------------------------------------------//
// inverse les valeurs des pixels de l'image en niveaux de gris X	//
// de taille h*l et renvoie le resultat dans Y.				//
//----------------------------------------------------------------------//

void Inverse(unsigned char *X, long h, long l, unsigned char *Y)
{
  int n_pixels = h * l;

  for (int i = 0; i < n_pixels; i++)
  {
    Y[i] = 255 - X[i];
  }
}

void Inverse_sse(unsigned char *X, long h, long l, unsigned char *Y)
{
  int n_pixels, i;
  __m128i vector_x, vector_res, vector_255;

  n_pixels = h * l;
  vector_255 = _mm_set1_epi8(255);

  for (i = 0; i < n_pixels; i += 16)
  {
    vector_x = _mm_load_si128((__m128i *)&X[i]);
    vector_res = _mm_sub_epi8(vector_255, vector_x);
    _mm_store_si128((__m128i *)&Y[i], vector_res);
  }
}

// Luminance_sse()
// -------------------------------------------------------------// reduit ou
// augmente de la valeur du parametre d_lum les valeurs des pixels de	//
// l'image en niveaux de gris X de taille h*l et renvoie le resultat dans Y.
// //
//------------------------------------------------------------------------------//

void Luminance_sse(int d_lum, unsigned char *X, long h, long l,
                   unsigned char *Y)
{
  int n_pixels, i;
  unsigned char d_lum_abs;
  __m128i vector_absolute, vector_value,vector_res;
  //valeur absolue de d_lum
  d_lum_abs = abs(d_lum);
  n_pixels = h * l;

  //affiche_xmm_i(vector_under_zero);
  //affiche_xmm_i(vector_over_zero);

  vector_absolute = _mm_set1_epi8(d_lum_abs);

  for (i = 0; i < n_pixels; i += 16)
  {
    // si la val de d_lum et sous 0 donc apres application du mask add_mask
    // sera un vecteur de 0 sinon 1 et le contraire si d_lum et au dessus de 0 ensuite le or permet de "fusioner" les resultas
    // juste un if c'est bien aussi
    vector_value = _mm_load_si128((__m128i *)&X[i]);
    if(d_lum >= 0){
      vector_res = _mm_adds_epu8(vector_value, vector_absolute);
    } else {
      vector_res = _mm_subs_epu8(vector_value, vector_absolute);
    }
    _mm_store_si128((__m128i *)&Y[i], vector_res);
  }
}


// binarise() ----------------------------------------------------------//
// binarise l'image en niveaux de gris X selon le parametre seuil	//
// et renvoie le resultat dans Y.					//
//----------------------------------------------------------------------//

void binarise(unsigned char seuil, unsigned char *X, long h, long l,
              unsigned char *Y)
{
  int n_pixels = h * l;

  for (int i = 0; i < n_pixels; i++)
  {
    Y[i] = (X[i] > seuil) ? 255 : 0;
  }
}

void binarise_sse(unsigned char seuil, unsigned char *X, long h, long l,
                  unsigned char *Y)
{
  int n_pixels, i;
  __m128i vector_x, vector_res, vector_seuil;

  n_pixels = h * l;
  vector_seuil = _mm_set1_epi8(seuil);

  for (i = 0; i < n_pixels; i += 16)
  {
    vector_x = _mm_load_si128((__m128i *)&X[i]);
    // si le la valeur est plus grande que le seuil alors elle devient le seuill sinon elle ne change pas
    // puis si la valeur est egale au seuil donc 255 (1111 1111)sinon 0 (0000 0000)
    vector_res = _mm_cmpeq_epi8(vector_seuil, _mm_min_epu8(vector_x, vector_seuil));
    _mm_store_si128((__m128i *)&Y[i], vector_res);
  }
}

// min_et_max()
// ----------------------------------------------------------------// calcule
// les valeurs min et max des pixels de l'image en niveaux de gris X.	//
//------------------------------------------------------------------------------//

void min_et_max(unsigned char *X, long h, long l, unsigned char &min,
                unsigned char &max)
{
  int n_pixels, i;

  n_pixels = h * l;
  min = 255;
  max = 0;

  for (i = 0; i < n_pixels; i++)
  {
    if (X[i] < min)
      min = X[i];
    if (X[i] > max)
      max = X[i];
  }
}

void min_et_max_sse(unsigned char *X, long h, long l, unsigned char &min,
                    unsigned char &max)
{
  int n_pixels, i;
  __m128i vector_x, vector_min, vector_max;
  unsigned char mins[16], maxs[16];

  n_pixels = h * l;
  min = 255;
  max = 0;

  vector_min = _mm_set1_epi8(255);
  vector_max = _mm_set1_epi8(0);
  // calcul les max et min 4 par 4
  for (i = 0; i < n_pixels; i += 16)
  {
    vector_x = _mm_load_si128((__m128i *)&X[i]);
    vector_min = _mm_min_epu8(vector_x, vector_min);
    vector_max = _mm_max_epu8(vector_x, vector_max);
  }
  _mm_store_si128((__m128i *)mins, vector_min);
  _mm_store_si128((__m128i *)maxs, vector_max);
  // puis les compare entre eux 
  for (i = 0; i < 16; i++)
  {
    min = (mins[i] < min) ? mins[i] : min;
    max = (maxs[i] > max) ? maxs[i] : max;
  }
}

// recadrageDynamique() ------------------------------------------------//
// calcule le recadrage dynamique de l'image en niveaux de gris X	//
// et renvoie le resultat dans Y					//
//----------------------------------------------------------------------//

void recadrageDynamique(unsigned char *X, long h, long l, unsigned char min,
                        unsigned char max, unsigned char *Y)
{
  long i;
  long size = h * l;

  // calcule des parametres pour le recadrage dynamique
  int div = max - min;

  for (i = 0; i < size; i++)
  {
    Y[i] = ((X[i] - min) * 255) / div;
  }
}


void recadrageDynamique_sse(unsigned char *X, long h, long l, unsigned char min,
                            unsigned char max, unsigned char *Y)
{
  long i, n_pixels;
  float div;
  __m128 vector_const_1, vector_const_2, vector_val_1, vector_val_2, vector_val_3, vector_val_4;
  __m128i vector_value,
      vector_zero,
      vector_unpacked_lo_16,
      vector_unpacked_hi_16,
      vector_unpacked_lo_32_1,
      vector_unpacked_lo_32_2,
      vector_unpacked_hi_32_1,
      vector_unpacked_hi_32_2;
      /*
      ((X[i] - min) * 255) / div;
      (X[i]*255 - min*255) /div
      X[i]*(255/div) - (min*255)/div
      
      on voit deux constante 
      (255/div) & (min*255/div)

      on les precalcule et on les met dans un registre chacun

      permet d'eviter la repetition de certain calcul
      */

  n_pixels = h * l;
  div = max - min;

  vector_const_1 = _mm_set1_ps(255.0 / div);
  vector_const_2 = _mm_set1_ps((min * 255.0) / div);

  vector_zero = _mm_setzero_si128();

  for (i = 0; i < n_pixels; i += 16)
  {
    // Charge les valeur dans un registre
    vector_value = _mm_load_si128((__m128i *)&X[i]);
    /*
    unpack progressive pour obtenir des valeur sur 32bit 
    de 8bit on passe a 16bit et de 16bit a 32bit 
    */
    vector_unpacked_lo_16 = _mm_unpacklo_epi8(vector_value, vector_zero);
    vector_unpacked_hi_16 = _mm_unpackhi_epi8(vector_value, vector_zero);
    vector_unpacked_lo_32_1 = _mm_unpacklo_epi16(vector_unpacked_lo_16, vector_zero);
    vector_unpacked_lo_32_2 = _mm_unpackhi_epi16(vector_unpacked_lo_16, vector_zero);
    vector_unpacked_hi_32_1 = _mm_unpacklo_epi16(vector_unpacked_hi_16, vector_zero);
    vector_unpacked_hi_32_2 = _mm_unpackhi_epi16(vector_unpacked_hi_16, vector_zero);
    // Conversion d'entier 32bit en flottant (simple precision) pour eviter des problemes d'overflow durant le calcul du resultat
    vector_val_1 = _mm_cvtepi32_ps(vector_unpacked_lo_32_1);
    vector_val_2 = _mm_cvtepi32_ps(vector_unpacked_lo_32_2);
    vector_val_3 = _mm_cvtepi32_ps(vector_unpacked_hi_32_1);
    vector_val_4 = _mm_cvtepi32_ps(vector_unpacked_hi_32_2);
    // Multiplication 
    vector_val_1 = _mm_mul_ps(vector_val_1, vector_const_1);
    vector_val_2 = _mm_mul_ps(vector_val_2, vector_const_1);
    vector_val_3 = _mm_mul_ps(vector_val_3, vector_const_1);
    vector_val_4 = _mm_mul_ps(vector_val_4, vector_const_1);
    // soustraction
    vector_val_1 = _mm_sub_ps(vector_val_1, vector_const_2);
    vector_val_2 = _mm_sub_ps(vector_val_2, vector_const_2);
    vector_val_3 = _mm_sub_ps(vector_val_3, vector_const_2);
    vector_val_4 = _mm_sub_ps(vector_val_4, vector_const_2);
    // Conversion des flottants en entier 32bit
    vector_unpacked_lo_32_1 = _mm_cvtps_epi32(vector_val_1);
    vector_unpacked_lo_32_2 = _mm_cvtps_epi32(vector_val_2);
    vector_unpacked_hi_32_1 = _mm_cvtps_epi32(vector_val_3);
    vector_unpacked_hi_32_2 = _mm_cvtps_epi32(vector_val_4);
    // Reduction successif des valeurs 32bit en valeur 16bit puis 8bit 
    vector_unpacked_lo_16 = _mm_packs_epi32(vector_unpacked_lo_32_1, vector_unpacked_lo_32_2);
    vector_unpacked_hi_16 = _mm_packs_epi32(vector_unpacked_hi_32_1, vector_unpacked_hi_32_2);
    vector_value = _mm_packus_epi16(vector_unpacked_lo_16, vector_unpacked_hi_16);
    // Ecriture du resultat dans le tableau destination
    _mm_store_si128((__m128i *)&Y[i], vector_value);
  }
}

int main(int argc, char **argv)
{

  unsigned short t[8] = {257,259,255,512,12,16,8192,1024};
  __m128i vector_test;
  vector_test = _mm_load_si128((__m128i*)t);
  affiche_xmm_i(vector_test);


  IplImage *img = NULL;

  if (argc != 2)
  {
    cerr << "Usage: " << argv[0] << " image" << endl;
    exit(1);
  }

  cout << "Ouverture de " << argv[1] << endl;
  img = cvLoadImage(
      argv[1], CV_LOAD_IMAGE_GRAYSCALE); // chargement en memoire de l'image
                                         // depuis le fichier passe en paramtre
  if (!img)
  {
    cerr << "Could not load image file: " << argv[1] << endl;
    exit(2);
  }
  cvShowImage("Affiche_origine", img);
  cout << argv[1] << " : " << img->height << "x" << img->width << endl;

  IplImage *image_inverse_sse = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
  Inverse_sse((unsigned char *)img->imageData, img->height, img->width, (unsigned char *)image_inverse_sse->imageData);
  cvShowImage("Affiche_inverse_sse", image_inverse_sse);

  IplImage *image_luminance = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
  Luminance_sse(64, (unsigned char *)img->imageData, img->height, img->width, (unsigned char *)image_luminance->imageData);
  cvShowImage("Affiche_luminace_sse", image_luminance);

  IplImage *image_luminance_neg = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
  Luminance_sse(-64, (unsigned char *)img->imageData, img->height, img->width, (unsigned char *)image_luminance_neg->imageData);
  cvShowImage("Affiche_luminace_sse_neg", image_luminance_neg);


  IplImage *image_binaire = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
  binarise_sse(120, (unsigned char *)img->imageData, img->height, img->width, (unsigned char *)image_binaire->imageData);
  cvShowImage("Affiche_binaire_sse", image_binaire);

  unsigned char min, max;
  min_et_max_sse((unsigned char *)img->imageData, img->height, img->width, min, max);
  cout << "min : " << (int)min << "   max : " << (int)max << endl;

  IplImage *image_ = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
  recadrageDynamique_sse((unsigned char *)img->imageData, img->height, img->width, min, max, (unsigned char *)image_->imageData);
  cvShowImage("Affiche_recadrage_dynamique_sse", image_);

  cvWaitKey(); // attend une touche clavier
  exit(0);
}
